#!/usr/bin/env bats

format_date() {
  date -d "$1" +%y.%m.%d
}

setup() {
    # get the containing directory of this file
    # use $BATS_TEST_FILENAME instead of ${BASH_SOURCE[0]} or $0,
    # as those will point to the bats executable's location or the preprocessed file respectively
    DIR="$( cd "$( dirname "$BATS_TEST_FILENAME" )" >/dev/null 2>&1 && pwd )"

    # make executables in .. visible to PATH
    PATH="$DIR/../:$PATH"

    # create a temporary working dir
    BATS_TMPDIR="$(mktemp -d)"

    # define our test configuration file
    TEST_CONF="${BATS_TMPDIR}"/archiver.conf

    LOG_FILE=${BATS_TMPDIR}/logfile
    PID_FILE=${BATS_TMPDIR}/pidfile
    NB_ARCHIVES_TO_KEEP=" [days]=2 [weeks]=3 "
    INCLUDES_FILE=${BATS_TMPDIR}/includes
    EXCLUDES_FILE=${BATS_TMPDIR}/excludes

    cat <<EOF > "${TEST_CONF}"
LOG_FILE=${LOG_FILE}
PID_FILE=${PID_FILE}
NB_ARCHIVES_TO_KEEP=(${NB_ARCHIVES_TO_KEEP})
INCLUDES_FILE=${INCLUDES_FILE}
EXCLUDES_FILE=${EXCLUDES_FILE}
EOF

    ROOT_BACKUP_DIR=${BATS_TMPDIR}/backup
    BACKUP_DIR=${ROOT_BACKUP_DIR}/myhost
    export -A BACKUP_DIR_BY_CONFIG=( [local]=${BACKUP_DIR} [ssh]=localhost:${BACKUP_DIR} [rsyncd]=localhost::backup/myhost )
    for config in ${!BACKUP_DIR_BY_CONFIG[@]} ; do
        cp "${TEST_CONF}" "${TEST_CONF}".$config
        echo "BACKUP_DIR=${BACKUP_DIR_BY_CONFIG[$config]}" >> "${TEST_CONF}".$config
    done
    if [ -z "${ALL_CONFIGS[@]}" ]
    then
        ALL_CONFIGS=( local )
    fi

    cat <<EOF > "${INCLUDES_FILE}"
${BATS_TMPDIR}/tobackup
EOF

    cat <<EOF > "${EXCLUDES_FILE}"
excludes
EOF

    PRE_ACTIONS=${BATS_TMPDIR}/pre_actions
    cat <<EOF > "${PRE_ACTIONS}"
touch ${BATS_TMPDIR}/pre_actions_done
EOF

    POST_ACTIONS=${BATS_TMPDIR}/post_actions
    cat <<EOF > "${POST_ACTIONS}"
touch ${BATS_TMPDIR}/post_actions_done
EOF

    if [[ "${ALL_CONFIGS[@]}" =~ rsyncd ]]
    then
        cat <<EOF > /etc/rsyncd.conf
port = 873
use chroot = yes

[backup]
  path = ${ROOT_BACKUP_DIR}
  comment = backup dir
  read only = false
  uid = 0
  gid = 0
EOF

        service rsync start
    fi

    # hack to stop on first failed test
    [ ! -f ${BATS_PARENT_TMPNAME}.skip ] || skip "skip remaining tests"
}

test_function() {
    local config=$1; shift
    bash -s <<EOF
source archiver.sh
source "${TEST_CONF}".$config
$@
EOF
}
test_cmd() {
    local config=$1; shift
    bash -x archiver.sh --config="${TEST_CONF}".$config $@
}

teardown() {
    if [[ "${ALL_CONFIGS[@]}" =~ rsyncd ]]
    then
        service rsync stop
    fi

    rm -rf "${BATS_TMPDIR}"

    # hack to stop on first failed test
    [ -n "$BATS_TEST_COMPLETED" ] || touch ${BATS_PARENT_TMPNAME}.skip
}

@test "get_all_backups" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/21.01.01/
        mkdir -p "${BACKUP_DIR}"/21.01.02/
        mkdir -p "${BACKUP_DIR}"/21.01.03/
        mkdir -p "${BACKUP_DIR}"/dummy_dir/
        touch "${BACKUP_DIR}"/21.01.01/FOO
        touch "${BACKUP_DIR}"/21.01.02/FOO
        touch "${BACKUP_DIR}"/21.01.03/FOO

        mapfile -t ALL_BACKUPS < <(test_function $config get_all_backups)
        echo ALL_BACKUPS[*]=${ALL_BACKUPS[*]} >&2
        [ "${ALL_BACKUPS[*]}" = "21.01.01 21.01.02 21.01.03" ]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "get_nb_backups" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/21.01.01/
        mkdir -p "${BACKUP_DIR}"/21.01.02/
        mkdir -p "${BACKUP_DIR}"/21.01.03/
        touch "${BACKUP_DIR}"/21.01.01/FOO
        touch "${BACKUP_DIR}"/21.01.02/FOO
        touch "${BACKUP_DIR}"/21.01.03/FOO

        NB_BACKUPS=$(test_function $config get_nb_backups)
        echo NB_BACKUPS=$NB_BACKUPS >&2
        [ "${NB_BACKUPS}" = "3" ]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "get_all_backups_done" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/21.01.01/
        mkdir -p "${BACKUP_DIR}"/21.01.02/
        mkdir -p "${BACKUP_DIR}"/21.01.03/
        mkdir -p "${BACKUP_DIR}"/latest/
        touch "${BACKUP_DIR}"/21.01.01/FOO
        touch "${BACKUP_DIR}"/21.01.02/FOO
        touch "${BACKUP_DIR}"/21.01.03/FOO
        touch "${BACKUP_DIR}"/latest/FOO
        touch "${BACKUP_DIR}"/21.01.01/.backup_done
        touch "${BACKUP_DIR}"/21.01.02/.backup_done # this one is the latest done

        mapfile -t ALL_BACKUPS_DONE < <(test_function $config get_all_backups_done)
        echo ALL_BACKUPS_DONE[*]=${ALL_BACKUPS_DONE[*]} >&2
        [ "${ALL_BACKUPS_DONE[*]}" = "21.01.01 21.01.02" ]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "get_all_backups_not_finished" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/21.01.01/
        mkdir -p "${BACKUP_DIR}"/21.01.02/
        mkdir -p "${BACKUP_DIR}"/21.01.03/
        mkdir -p "${BACKUP_DIR}"/21.01.04/
        mkdir -p "${BACKUP_DIR}"/latest/
        touch "${BACKUP_DIR}"/21.01.01/FOO
        touch "${BACKUP_DIR}"/21.01.02/FOO
        touch "${BACKUP_DIR}"/21.01.03/FOO
        touch "${BACKUP_DIR}"/21.01.04/FOO
        touch "${BACKUP_DIR}"/latest/FOO
        touch "${BACKUP_DIR}"/21.01.01/.backup_done
        touch "${BACKUP_DIR}"/21.01.02/.backup_done # this one is the latest done

        mapfile -t ALL_BACKUPS_NOT_FINISHED < <(test_function $config get_all_backups_not_finished)
        echo ALL_BACKUPS_NOT_FINISHED[*]=${ALL_BACKUPS_NOT_FINISHED[*]} >&2
        [ "${ALL_BACKUPS_NOT_FINISHED[*]}" = "21.01.04 21.01.03" ]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "get_last_backup_done" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/21.01.01/
        mkdir -p "${BACKUP_DIR}"/21.01.02/
        mkdir -p "${BACKUP_DIR}"/21.01.03/
        mkdir -p "${BACKUP_DIR}"/latest/
        touch "${BACKUP_DIR}"/21.01.01/FOO
        touch "${BACKUP_DIR}"/21.01.02/FOO
        touch "${BACKUP_DIR}"/21.01.03/FOO
        touch "${BACKUP_DIR}"/latest/FOO
        touch "${BACKUP_DIR}"/21.01.01/.backup_done
        touch "${BACKUP_DIR}"/21.01.02/.backup_done # this one is the latest done

        LAST_BACKUP_DONE=$(test_function $config get_last_backup_done)
        echo LAST_BACKUP_DONE=$LAST_BACKUP_DONE >&2
        [ "${LAST_BACKUP_DONE}" = "21.01.02" ]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "get_backup" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/21.01.01/
        mkdir -p "${BACKUP_DIR}"/21.01.02/
        mkdir -p "${BACKUP_DIR}"/21.01.03/

        BACKUP=$(test_function $config get_backup 21.01.01)
        echo BACKUP=$BACKUP >&2
        [ "${BACKUP}" = "${BACKUP_DIR_BY_CONFIG[$config]}"/"21.01.01" ]

        BACKUP_NOT_EXIST=$(test_function $config get_backup 121.01.01)
        echo BACKUP_NOT_EXIST=$BACKUP_NOT_EXIST >&2
        [ "${BACKUP_NOT_EXIST}" = "" ]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "file_exist" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/21.01.01/
        mkdir -p "${BACKUP_DIR}"/21.01.02/
        mkdir -p "${BACKUP_DIR}"/21.01.03/

        test_function $config file_exist "${BACKUP_DIR}"/21.01.01

        ! test_function $config file_exist "${BACKUP_DIR}"/121.01.01

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "del_archives" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/21.01.01/
        mkdir -p "${BACKUP_DIR}"/21.01.02/
        mkdir -p "${BACKUP_DIR}"/21.01.03/

        test_function $config del_archives 21.01.01 21.01.02

        [ ! -d "${BACKUP_DIR}"/21.01.01/ ]
        [ ! -d "${BACKUP_DIR}"/21.01.02/ ]
        [ -d "${BACKUP_DIR}"/21.01.03/ ]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "clean" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/$(format_date "-7 days")/
        mkdir -p "${BACKUP_DIR}"/$(format_date "-6 days")/
        mkdir -p "${BACKUP_DIR}"/$(format_date "-5 days")/
        mkdir -p "${BACKUP_DIR}"/$(format_date "-4 days")/
        mkdir -p "${BACKUP_DIR}"/$(format_date "-3 days")/
        mkdir -p "${BACKUP_DIR}"/$(format_date "-2 days")/
        mkdir -p "${BACKUP_DIR}"/$(format_date "-1 days")/
        mkdir -p "${BACKUP_DIR}"/$(format_date "0 days")/

        test_cmd $config clean

        [ -d "${BACKUP_DIR}"/$(format_date "-7 days")/ ] # kept as 1 week old backup
        [ ! -d "${BACKUP_DIR}"/$(format_date "-6 days")/ ]
        [ ! -d "${BACKUP_DIR}"/$(format_date "-5 days")/ ]
        [ ! -d "${BACKUP_DIR}"/$(format_date "-4 days")/ ]
        [ ! -d "${BACKUP_DIR}"/$(format_date "-3 days")/ ]
        [ -d "${BACKUP_DIR}"/$(format_date "-2 days")/ ]
        [ -d "${BACKUP_DIR}"/$(format_date "-1 days")/ ]
        [ -d "${BACKUP_DIR}"/$(format_date "0 days")/ ]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "archive" {
    for config in ${ALL_CONFIGS[@]}
    do
        TODAY=$(format_date "0 days")
        YESTERDAY=$(format_date "-1 days")

        # create dummy source files
        mkdir -p ${BATS_TMPDIR}/tobackup
        touch ${BATS_TMPDIR}/tobackup/FOO
        touch ${BATS_TMPDIR}/tobackup/BAR

        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir "${ROOT_BACKUP_DIR}"

        # initial setup is needed before calling archive
        test_cmd $config setup

        # test initial archive creation
        test_cmd $config archive

        [ -f "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/FOO ]
        [ -f "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/BAR ]
        [ -f "${BACKUP_DIR}"/${TODAY}/.backup_done ]

        # check that command list returns the newly created archive
        NEW_ARCHIVE=$(test_cmd $config list)
        [ "${NEW_ARCHIVE}" = "${TODAY}" ]

        # test another call does not work without --force
        touch ${BATS_TMPDIR}/tobackup/FOOBAR
        test_cmd $config archive
        [ ! -f "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/FOOBAR ]
        # and now works with --force
        test_cmd $config archive --force
        [ -f "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/FOOBAR ]

        # test with existing repository
        mv "${BACKUP_DIR}"/${TODAY} "${BACKUP_DIR}"/${YESTERDAY}
        test_cmd $config archive
        [ -f "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/FOO ]
        [ -f "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/BAR ]
        [ -f "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/FOOBAR ]
        [ -f "${BACKUP_DIR}"/${TODAY}/.backup_done ]

        # check the FOO file is a hardlink to the previous one
        TODAY_INODE=$(ls -i "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/FOO | cut -d ' ' -f 1)
        YESTERDAY_INODE=$(ls -i "${BACKUP_DIR}"/${YESTERDAY}/${BATS_TMPDIR}/tobackup/FOO | cut -d ' ' -f 1)
        echo TODAY_INODE=${TODAY_INODE} >&2
        echo YESTERDAY_INODE=${YESTERDAY_INODE} >&2
        [ "${TODAY_INODE}" = "${YESTERDAY_INODE}" ]

        # test with pre_archive_actions
        echo "PRE_ACTIONS=${PRE_ACTIONS}" >> "${TEST_CONF}".$config
        [ ! -f ${BATS_TMPDIR}/pre_actions_done ]
        test_cmd $config archive --force
        [ -f ${BATS_TMPDIR}/pre_actions_done ]

        # test with post_archive_actions
        echo "POST_ACTIONS=${POST_ACTIONS}" >> "${TEST_CONF}".$config
        [ ! -f ${BATS_TMPDIR}/post_actions_done ]
        test_cmd $config archive --force
        [ -f ${BATS_TMPDIR}/post_actions_done ]

        rm -fr "${ROOT_BACKUP_DIR}"
        rm -fr "${BATS_TMPDIR}"/tobackup
        rm -fr "${BATS_TMPDIR}"/pre_actions_done
        rm -fr "${BATS_TMPDIR}"/post_actions_done
    done
}

@test "list_archive" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/21.01.01/
        mkdir -p "${BACKUP_DIR}"/21.01.02/
        mkdir -p "${BACKUP_DIR}"/21.01.03/
        mkdir -p "${BACKUP_DIR}"/latest/
        touch "${BACKUP_DIR}"/21.01.01/FOO
        touch "${BACKUP_DIR}"/21.01.02/BAR
        touch "${BACKUP_DIR}"/21.01.03/FOOBAR
        touch "${BACKUP_DIR}"/latest/FOO
        touch "${BACKUP_DIR}"/21.01.01/.backup_done
        touch "${BACKUP_DIR}"/21.01.02/.backup_done # this one is the latest done

        CONTENT=$(test_cmd $config list_archive 21.01.01)
        echo CONTENT=$CONTENT >&2

        [[ "$CONTENT" =~ FOO ]]

        CONTENT=$(test_cmd $config list_archive 21.01.02)
        echo CONTENT=$CONTENT >&2

        [[ "$CONTENT" =~ BAR ]]

        CONTENT=$(test_cmd $config list_archive 21.01.03)
        echo CONTENT=$CONTENT >&2

        [[ "$CONTENT" =~ FOOBAR ]]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "restore" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/21.01.01/
        mkdir -p "${BACKUP_DIR}"/21.01.02/
        mkdir -p "${BACKUP_DIR}"/21.01.03/
        mkdir -p "${BACKUP_DIR}"/latest/
        touch "${BACKUP_DIR}"/21.01.01/FOO
        touch "${BACKUP_DIR}"/21.01.02/BAR
        touch "${BACKUP_DIR}"/21.01.03/FOOBAR
        touch "${BACKUP_DIR}"/latest/FOO
        touch "${BACKUP_DIR}"/21.01.01/.backup_done
        touch "${BACKUP_DIR}"/21.01.02/.backup_done # this one is the latest done

        test_cmd $config restore 21.01.02 ${BATS_TMPDIR}/restore_path/

        [ -f ${BATS_TMPDIR}/restore_path/BAR ]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "setup" {
    for config in ${ALL_CONFIGS[@]}
    do
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir "${ROOT_BACKUP_DIR}"

        test_cmd $config setup

        [ -d ${BACKUP_DIR} ]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "clean_not_finished" {
    for config in ${ALL_CONFIGS[@]}
    do
        # create a dummy existing backup repository
        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"/21.01.01/
        mkdir -p "${BACKUP_DIR}"/21.01.02/
        mkdir -p "${BACKUP_DIR}"/21.01.03/
        mkdir -p "${BACKUP_DIR}"/latest/
        touch "${BACKUP_DIR}"/21.01.01/FOO
        touch "${BACKUP_DIR}"/21.01.02/BAR
        touch "${BACKUP_DIR}"/21.01.03/FOOBAR
        touch "${BACKUP_DIR}"/latest/FOO
        touch "${BACKUP_DIR}"/21.01.01/.backup_done
        touch "${BACKUP_DIR}"/21.01.02/.backup_done # this one is the latest done

        test_cmd $config clean_not_finished

        [ -d "${BACKUP_DIR}"/21.01.01 ]
        [ -d "${BACKUP_DIR}"/21.01.02 ]
        [ ! -d "${BACKUP_DIR}"/21.01.03 ]

        rm -fr "${ROOT_BACKUP_DIR}"
    done
}

@test "archive_interrupted" {
    for config in ${ALL_CONFIGS[@]}
    do
        TODAY=$(format_date "0 days")
        YESTERDAY=$(format_date "-1 days")

        # create dummy source files
        mkdir -p ${BATS_TMPDIR}/tobackup
        touch ${BATS_TMPDIR}/tobackup/FOO
        touch ${BATS_TMPDIR}/tobackup/BAR

        rm -fr "${ROOT_BACKUP_DIR}"
        mkdir "${ROOT_BACKUP_DIR}"

        # initial setup is needed before calling archive
        test_cmd $config setup

        # test initial archive creation
        test_cmd $config archive

        [ -f "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/FOO ]
        [ -f "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/BAR ]
        [ -f "${BACKUP_DIR}"/${TODAY}/.backup_done ]

        # test with existing repository
        mv "${BACKUP_DIR}"/${TODAY} "${BACKUP_DIR}"/${YESTERDAY}
        test_cmd $config archive
        [ -f "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/FOO ]
        [ -f "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/BAR ]
        [ -f "${BACKUP_DIR}"/${TODAY}/.backup_done ]

        # simulate an interrupted archive (yesterday and today)
        rm "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/FOO
        rm "${BACKUP_DIR}"/${YESTERDAY}/.backup_done
        rm "${BACKUP_DIR}"/${TODAY}/.backup_done

        # and continue the archive
        test_cmd $config archive

        # check the FOO file is a hardlink to the previous one
        TODAY_INODE=$(ls -i "${BACKUP_DIR}"/${TODAY}/${BATS_TMPDIR}/tobackup/FOO | cut -d ' ' -f 1)
        YESTERDAY_INODE=$(ls -i "${BACKUP_DIR}"/${YESTERDAY}/${BATS_TMPDIR}/tobackup/FOO | cut -d ' ' -f 1)
        echo TODAY_INODE=${TODAY_INODE} >&2
        echo YESTERDAY_INODE=${YESTERDAY_INODE} >&2
        [ "${TODAY_INODE}" = "${YESTERDAY_INODE}" ]

        rm -fr "${ROOT_BACKUP_DIR}"
        rm -fr "${BATS_TMPDIR}"/tobackup
    done
}

