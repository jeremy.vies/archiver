Description
===========

Archiver is a simple rsync based backup script.
It uses rsync with incremental copies to save space and speed-up the archiving.
It does not compress, nor encrypt the archive. It makes easier to manually restore a file.

The archives repository path can be either local (&lt;absolute path&gt;), using rsync over ssh (&lt;hostname&gt;:&lt;backup_dir&gt;) or using rsyncd (&lt;hostname&gt;::&lt;rsyncd_service_name&gt;/&lt;backup_dir&gt;).
All commands are available in the 3 modes.

Installation
============

The sources embed a debian packaging configuration.

For a manual installation, please copy the following files at the relevant places:

archiver.sh => /usr/bin/archiver.sh
etc/archiver/* => /etc/archiver/
etc/cron.daily/* => etc/cron.daily/

Configuration
=============

The configuration is done in /etc/archiver/archiver.conf. The file is a bash script file sourced by the main script.

Most common settings to configure are:
* BACKUP_DIR: where archives are kept (default to /mnt/backup/${HOSTNAME})
* MOUNT: mount command to run before any action on the archives repository (default to nothing)
* UMOUNT: umount command to run after any action on the archives repository (default to nothing)
* INCLUDES_FILE: path to the list of archived files (default to ${CONFIG_DIR}/archived_files)
* EXCLUDES_FILE: path to the list of excluded files (default to ${CONFIG_DIR}/excluded_from_archive)
* PRE_ACTIONS: path to the shell script run before the archive creation (default to ${CONFIG_DIR}/pre_archive_actions)
* POST_ACTIONS: path to the shell script run after the archive creation (default to ${CONFIG_DIR}/post_archive_actions)

NB: the variable CONFIG_DIR is set by the main script as the folder containing the provided configuration file.

Less common settings to configure are:
* NB_ARCHIVES_TO_KEEP: the backup retention policy (default to ([days]=6 [weeks]=4 [months]=12 [years]=2) )
* LOG_FILE: the archiver log file (default to /var/log/archiver.log)
* PID_FILE: the archviver pid file (default to /var/run/archiver.pid)

Command Line
============

The script should be installed in /usr/bin/archiver.sh.
It supports the following commands:

* clean: clean old archives
* list: list available archives
* list_archive <archive_date>: list content of archive <archive_date>
* restore <archive_date> <location>: restore the archive <archive_date> into <location>
* archive: make a new archive
* setup: create a new archive directory
* clean_not_finished: removed not completely done archives
* --force: enforce archiving
* --verbose: let rsync be more verbose
* --config=<config_file>: specify a config file. default is /etc/archiver/archiver.conf

