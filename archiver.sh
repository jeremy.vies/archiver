#!/bin/bash

# get script path and name
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
SCRIPT_NAME=$(basename "$0")

# bash options
set -o pipefail

# default values
########################################################################
if [ -f "${SCRIPT_DIR}"/etc/archiver ]
then
  CONFIG_DIR="${SCRIPT_DIR}"/etc/archiver
else
  CONFIG_DIR=/etc/archiver
fi
CONFIG_FILE=${CONFIG_DIR}/archiver.conf
BACKUP_DIR=/mnt/backup/${HOSTNAME}
# number of archives to keep (only days weeks months years are supported)
HANDLED_PERIODS=(days weeks months years)
declare -A NB_ARCHIVES_TO_KEEP=([days]=6 [weeks]=4 [months]=12 [years]=2)
RSYNC="rsync"
# files to backup
INCLUDES_FILE=${CONFIG_DIR}/archived_files
# excludes file - this contains a wildcard pattern per line of files to exclude
EXCLUDES_FILE=${CONFIG_DIR}/excluded_from_archive
# pre/post-archive actions
PRE_ACTIONS=${CONFIG_DIR}/pre_archive_actions
POST_ACTIONS=${CONFIG_DIR}/post_archive_actions
# log file
LOG_FILE=/var/log/archiver.log
# pid file
PID_FILE=/var/run/archiver.pid
# backup done file
DONE_FILE=.backup_done
########################################################################


# functions
########################################################################

function log_debug() {
  if [ -n "${DEBUG}" ]
  then
    echo -ne "${SCRIPT_NAME} $(date --rfc-3339=seconds) DEBUG: $*\n"
  fi
}
function log_info() {
  echo -ne "${SCRIPT_NAME} $(date --rfc-3339=seconds) INFO: $*\n" | tee -a ${LOG_FILE} 
}
function log_warning() {
  echo -ne "${SCRIPT_NAME} $(date --rfc-3339=seconds) WARNING: $*\n" | tee -a ${LOG_FILE} 1>&2
}
function log_error() {
  echo -ne "${SCRIPT_NAME} $(date --rfc-3339=seconds) ERROR: $*\n" | tee -a ${LOG_FILE} 1>&2
}
function log_fatal() {
  echo -ne "${SCRIPT_NAME} $(date --rfc-3339=seconds) FATAL: $*\n" | tee -a ${LOG_FILE} 1>&2 ; exit 1
}

function get_all_backups() {
    # get content of BACKUP_DIR ...
    ${RSYNC} "${BACKUP_DIR}"/ 2>/dev/null | \
      # , get last field ...
      awk '{ print $NF }' | \
      # , filter according to regex
      grep -E "^[0-9]{2}\.[0-9]{2}\.[0-9]{2}$"
}

function get_nb_backups() {
    get_all_backups | wc -l
}

function get_all_backups_done() {
  local backup
  for backup in $( get_all_backups )
  do
    if file_exist "${BACKUP_DIR}/${backup}/${DONE_FILE}"
    then
      echo "${backup}"
    fi
  done
}

function get_last_backup_done() {
    get_all_backups_done | sort -r | head -n 1
}

function get_all_backups_not_finished() {
  local backup
  for backup in $( get_all_backups | sort -r )
  do
    if ! file_exist "${BACKUP_DIR}/${backup}/${DONE_FILE}"
    then
      echo "${backup}"
    fi
  done
}

function get_backup() {
  if [ -z "$1" ]
  then
    log_fatal "no archive specified !"
  fi
  local backup="${BACKUP_DIR}"/"$1"
  if file_exist "${backup}"
  then
    echo "${backup}"
  else
    log_error "archive $1 does not exist"
  fi
}

function file_exist() {
  ${RSYNC} "$1" &>/dev/null
  return $?
}

function remove_pid() {
  if [ -n "${PID_FILE}" ]
  then
    rm -f ${PID_FILE}
  fi
}

function del_archives()
{
  local archive
  local -a includes_pattern=()
  for archive in "$@"
  do
    includes_pattern+=(--include "${archive}"/***)
  done
  local empty_dir
  empty_dir=$(mktemp -d)
  ${RSYNC} --recursive --delete --force "${includes_pattern[@]}"  --exclude '*' "${empty_dir}"/ "${BACKUP_DIR}"/
  rmdir "${empty_dir}"
}

function clean_old_archives() {
  log_debug "cleaning"

  local -a archives_to_check
  mapfile -t archives_to_check < <( get_all_backups | sort -r )

  local -a tmp_list=()
  local -a archives_to_keep=()
  local -a archives_to_remove=()
  local d
  local i
  local period

  # build the archives_to_remove list
  # loop on each period
  for period in "${HANDLED_PERIODS[@]}"
  do
    # for each period, lets find the entry to keep
    for (( i=0; i < ${NB_ARCHIVES_TO_KEEP[$period]} + 1 ; i++ ))
    do
      # lets build the list of all entries that matches the period rule
      for d in ${archives_to_check[*]}
      do
        if [[ $d > $(date "+%y.%m.%d" -d "-$i ${period}") || $d == $(date "+%y.%m.%d" -d "-$i ${period}") ]]
        then
          tmp_list+=("$d")
        else
          # as the archive list is sorted, we can stop as soon as one does not match
          break
        fi
      done
      # we keep only the last entry that matches the period rule
      if (( ${#tmp_list[@]} > 0))
      then
        local archive_to_keep=${tmp_list[ (( ${#tmp_list[@]} - 1 )) ]}
        log_info "${archive_to_keep} kept as the $i ${period}"

        archives_to_keep+=("${archive_to_keep}")
        archives_to_remove+=("${tmp_list[@]/${archive_to_keep}}")

        for d in "${tmp_list[@]}"
        do
          archives_to_check=("${archives_to_check[@]/$d}")
        done

        unset tmp_list
      fi
    done
  done

  archives_to_remove+=("${archives_to_check[@]}")


  # delete old archives
  log_info "ARCHIVES REMOVED ${archives_to_remove[*]}"
  del_archives "${archives_to_remove[@]}"
}

function new_archive() {
  log_debug "archiving"

  local today
  today=$(date +"%y.%m.%d")
  local new_archive="${BACKUP_DIR}/${today}"
  log_debug "new_archive=${new_archive}"

  # check if it needs to be archived
  if [ -z "${FORCE}" ] && file_exist "${new_archive}/${DONE_FILE}"
  then
    log_debug "Host is already archived. skipping."
    return
  fi

  # log starting
  log_info "*** starting new_archive on ${BACKUP_DIR} ***"

  # do pre-actions
  if [ -f "${PRE_ACTIONS}" ]
  then
    log_debug "starting pre-actions"
    new_archive=${new_archive} bash -s < ${PRE_ACTIONS}
    log_debug "end of pre-actions"
  fi

  # find last archive
  local last_archive
  last_archive=$(get_last_backup_done)
  if [ -z "${last_archive}" ]
  then
    mapfile -t < <( get_all_backups | sort -r | grep -v "${today}" )
    last_archive=${MAPFILE[0]}
  fi
  if [ -n "${last_archive}" ]
  then
    local LINK_OPT=(--link-dest=../"${last_archive}" --ignore-existing)
  fi
  log_debug "LINK_OPT=${LINK_OPT[*]}"

  # options for rsync
  local -a opts
  opts=(--force --ignore-errors --delete-excluded --exclude-from="${EXCLUDES_FILE}"
    --delete-delay "${LINK_OPT[@]}" --archive --recursive --human-readable --stats --partial --partial-dir partial --hard-links)
  # shellcheck disable=SC2206
  opts+=(${VERBOSE})
  log_debug "opts=${opts[*]}"

  # copy
  log_debug "calling ${RSYNC} ${opts[*]} --files-from=${INCLUDES_FILE} / ${new_archive} 2>&1 | tee -a ${LOG_FILE}"
  ${RSYNC} "${opts[@]}" --files-from=${INCLUDES_FILE} / "${new_archive}" 2>&1 | tee -a ${LOG_FILE}
  local error_code=$?

  # create done file
  if [[ $error_code -eq 0 ]] || [[ $error_code -eq 23 ]] || [[ $error_code -eq 24 ]]
  then
    local EMPTY_FILE
    EMPTY_FILE=$(mktemp)
    ${RSYNC} "${EMPTY_FILE}" "${new_archive}"/${DONE_FILE}
    rm -f "${EMPTY_FILE}"
  else
    log_error "archive is partial ! (return code=$?)"
  fi

  # do post-actions
  if [ -f "${POST_ACTIONS}" ]
  then
    log_debug "starting post-actions"
    new_archive=${new_archive} bash -s < ${POST_ACTIONS}
    log_debug "end of post-actions"
  fi

  # log starting
  log_info "*** ending new_archive ***"
}

function list_archive() {
  local backup
  backup=$(get_backup "$1")

  if [ -n "${backup}" ]
  then
    echo "archive $1 contains:"

    # echo content of archive
    ${RSYNC} "${backup}"/
  else
    get_all_backups
  fi
}

function restore_archive() {
  local backup
  backup=$(get_backup "$1")

  log_debug "restoring $1..."
  local opts=(--force --ignore-errors --archive --recursive --human-readable --stats --compress --exclude "${DONE_FILE}")
  # shellcheck disable=SC2206
  opts+=(${VERBOSE})

  ${RSYNC} "${opts[@]}" "${backup}"/ "$2" 2>&1 | tee -a ${LOG_FILE}
}

function setup() {
  local empty_dir
  empty_dir=$(mktemp -d)
  ${RSYNC} "${empty_dir}"/ "${BACKUP_DIR}"/
  rmdir "${empty_dir}"
}

function clean_not_finished() {
  del_archives "$( get_all_backups_not_finished )"
}

function usage() {
  echo "Usage :"
  echo "$0 cmd [cmd_args] [--verbose]"
  echo "cmd may be:"
  echo "  clean: clean old archives"
  echo "  list: list available archives"
  echo "  list_archive <archive_date>: list content of archive"
  echo "  restore <archive_date><restore_path>: restore an archive"
  echo "  archive: make a new archive"
  echo "  setup: create a new archive directory"
  echo "  clean_not_finished: removed not completely done archives"
  echo "--force: enforce archiving"
  echo "--verbose: let rsync be more verbose"
  echo "--config=<config_file>: specify a config file. default is ${CONFIG_FILE}"
}

function main() {
    # do not allow any error
    set -e
    local cmd

    # parse args
    local -a positional=()
    while [[ $# -gt 0 ]]; do
      local key="$1"
      case $key in
        ("clean") # clean
        cmd=${cmd:-clean_old_archives}
        ;;
        ("list") # list available archives
        cmd=${cmd:-get_all_backups}
        ;;
        ("list_archive") # list content of an archive
        cmd=${cmd:-list_archive}
        ;;
        ("restore") # restore an archive
        cmd=${cmd:-restore_archive}
        ;;
        ("archive") # create a new archive
        cmd=${cmd:-new_archive}
        ;;
        ("setup") # create a new archive directory
        cmd=${cmd:-setup}
        ;;
        ("clean_not_finished")
        cmd=${cmd:-clean_not_finished}
        ;;
        ("-h"|"--help"|"help") # help
        usage
        exit 0
        ;;
        (--debug) # ask for debug (no rm, no rsync)
        DEBUG=1
        RSYNC="log_debug ${RSYNC}"
        ;;
        (--force) # enforce archiving
        FORCE=1
        ;;
        ("-v"|"--verbose") # be verbose
        VERBOSE=--verbose
        ;;
        (--config=*) # specify a specific config file
        CONFIG_FILE=${key#*=}
        ;;
        (*)
        positional+=("$1")
        ;;
      esac
      shift
    done
    set -- "${positional[@]}" # restore positional parameters

    # get values from config file
    if [ -f "${CONFIG_FILE}" ]
    then
      CONFIG_DIR=$(dirname "${CONFIG_FILE}")
      # shellcheck source=etc/archiver/archiver.conf
      source "${CONFIG_FILE}"
    else
      log_fatal "unable to find specified config file : ${CONFIG_FILE}"
    fi

    # check we have a command
    if [ -z "${cmd}" ]
    then
      usage
      exit 1
    fi


    # check for existing pid
    if [ -n "${PID_FILE}" ] && [ -f "${PID_FILE}" ]
    then
      if [ -d /proc/"$(cat "${PID_FILE}")" ]
      then
        log_warning "archiver already running with pid $(cat "${PID_FILE}")"
        exit
      else
        log_debug "removing old pid file"
        rm -f "${PID_FILE}"
      fi
    fi

    # store pid
    if [ -n "${PID_FILE}" ]
    then
      echo $$ > "${PID_FILE}"
    fi

    # remove pid on signal ERR
    trap remove_pid ERR

    # do mount if needed
    ${MOUNT}

    # run the commands
    ${cmd} "$@"

    # do umount if needed
    ${UMOUNT}

    # remove pid
    remove_pid
}

# main code
########################################################################
if [ "${BASH_SOURCE[0]}" = "$0" ]
then
    main "$@"
fi

